import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router'
import { FootballHomeComponent } from './football-home/football-home.component';
import { NgxChartsModule } from '../../node_modules/@swimlane/ngx-charts';
import { FootballDefenseComponent } from './football-defense/football-defense.component';



const routes: Routes = [
  { path: 'global-football', component: FootballHomeComponent },
  { path : 'defensive-ranking' , component : FootballDefenseComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), NgxChartsModule],
  exports: [RouterModule]

})
export class AppRoutingModule { }
