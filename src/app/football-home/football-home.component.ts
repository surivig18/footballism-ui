import { Component, OnInit } from '@angular/core';
import {FootballService} from '../football-service/football-service.service';
import{RankingModel} from '../../model/ranking-model'
import {SeriesData,TransformedRankingModel} from '../../model/transformed-ranking-model';

@Component({
  selector: 'app-football-home',
  templateUrl: './football-home.component.html',
  styleUrls: ['./football-home.component.css']
})
export class FootballHomeComponent implements OnInit {

  private jsonData : any;
  private rankingModels: RankingModel[];
  private resultData : Object[] =[];
  private dataLoad = false;
  multi : Object;


  view: any[] = [1200, 600];

  // options for the chart

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Teams';
  showYAxisLabel = true;
  yAxisLabel = 'Ranking';
  timeline = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

  //pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  constructor(private footballService : FootballService) { 
    


  }

  ngOnInit() {
    this.invokeSerive();

  }
  invokeSerive(){
  return new Promise((resolve) => {
  let rankingModels :RankingModel[] 
  
    this.footballService.getRankingData().subscribe(e => 
    {
      console.log(e.json());
      this.jsonData = e.json();
       rankingModels = <RankingModel[]> this.jsonData;
      this.rankingModels = rankingModels;
      this.transformData();
      console.log(this.resultData);
      this.multi = this.resultData;
      console.log(this.multi)
      this.dataLoad = true;


    });
    resolve();

  });

}
 transformData(){
console.log("hereeeee")
  let leagueMap  = new Map<String,TransformedRankingModel>();
  console.log(this.rankingModels["rankingModels"])
  this.rankingModels["rankingModels"].forEach( e => { 
    console.log(e)

    let transformedData : TransformedRankingModel;

    if(leagueMap.has(e.league))
    {
       transformedData  = leagueMap.get(e.league);
       let series : SeriesData ={
        name : e.name,
        value : e.rank
      }
       transformedData.series.push(series)
    }
    else{
      let series : SeriesData ={
        name : e.name,
        value : e.rank
      }
     
      let seriesData : SeriesData [] = [];
      seriesData.push(series)
      transformedData  = {
        name : e.league,
        series : seriesData
      }
      leagueMap.set(e.league,transformedData);
      this.resultData.push(transformedData);

    }

        console.log("hereeee")
  });

}
// data goes here


}

