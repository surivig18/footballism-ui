import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Football rankings';
  navLinks = [
    {'title' : 'Overall ranking',
     'url' : '/global-football'},
    {'title' : 'Defensive ranking',
    'url' : '/defensive-ranking'},
    {'title' : 'Offensive ranking',
    'url' : '/global-football'}]
}
