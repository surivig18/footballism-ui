import { Injectable } from '@angular/core';
import {Http} from '@angular/http'
import {environment} from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class FootballService {

  constructor(private http: Http) { }

  getRankingData(){
    return this.http.get(environment.footbalRankinglUrl);
  }
}
