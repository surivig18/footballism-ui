import { Component, OnInit } from '@angular/core';
import {FootballService} from '../football-service/football-service.service';
import{RankingModel} from '../../model/ranking-model'
import {TransformedRankingModel,SeriesData} from '../../model/transformed-ranking-model';

@Component({
  selector: 'app-football-defense',
  templateUrl: './football-defense.component.html',
  styleUrls: ['./football-defense.component.css']
})
export class FootballDefenseComponent implements OnInit {


  private rankingModels : RankingModel[];
  private filteredList : RankingModel[];
  private resultData : TransformedRankingModel[] = [];
  private leagueFilters = ['Barclays Premier League' , 'German Bundesliga' ,'Spanish Primera Division', 'French Ligue 1'];
  private graphData : {};
  multi : Object;


  view: any[] = [1200, 600];

  // options for the chart

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Teams';
  showYAxisLabel = true;
  yAxisLabel = 'Spi';
  timeline = true;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;
  constructor(private footballService : FootballService) { 

   }
  

  ngOnInit() {
    this.setupLoadData().then(() => {
      console.log(this.rankingModels)
        this.filteredList = this.rankingModels.filter(
          rankingModel =>  this.leagueFilters.includes(rankingModel.league)
        );
        console.log(this.filteredList);
        this.transformData();
        this.multi = this.resultData;


    });
  }

  setupLoadData(){
      return new Promise( (resolve,reject) => {
        this.footballService.getRankingData().subscribe(
          response =>{
             let result = response.json();
             this.rankingModels = result["rankingModels"];
            resolve();
          });
      });
  }

  transformData(){
      let leagueMap  = new Map<String,TransformedRankingModel>();
      this.filteredList.forEach( rankingModel => { 
    
        let transformedData : TransformedRankingModel;
    
        if(leagueMap.has(rankingModel.league))
        {
           transformedData  = leagueMap.get(rankingModel.league);
           let series : SeriesData ={
            name : rankingModel.name,
            value : rankingModel.def
          }
           transformedData.series.push(series)
        }
        else{
          let series : SeriesData ={
            name : rankingModel.name,
            value : rankingModel.def
          }
         
          let seriesData = [];
          seriesData.push(series)
          transformedData  = {
            name : rankingModel.league,
            series : seriesData
          }
          leagueMap.set(rankingModel.league,transformedData);
          this.resultData.push(transformedData);
    
        }
    
      });
    
  }

}
