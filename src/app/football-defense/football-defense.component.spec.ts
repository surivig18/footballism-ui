import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FootballDefenseComponent } from './football-defense.component';

describe('FootballDefenseComponent', () => {
  let component: FootballDefenseComponent;
  let fixture: ComponentFixture<FootballDefenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FootballDefenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FootballDefenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
