import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { FootballHomeComponent } from './football-home/football-home.component';
import { AppRoutingModule } from './/app-routing.module';
import { NgxChartsModule } from '../../node_modules/@swimlane/ngx-charts';
import {HttpModule} from '@angular/http'
import { FootballService } from './football-service/football-service.service';
import {MatGridListModule} from '@angular/material/grid-list'
import {MatTabsModule} from '@angular/material/tabs';
import { FootballDefenseComponent } from './football-defense/football-defense.component';

@NgModule({
  declarations: [
    AppComponent,
    FootballHomeComponent,
    FootballDefenseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxChartsModule,
    HttpModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatTabsModule
  ],
  exports:[
    MatGridListModule
  ],
  providers: [FootballService],
  bootstrap: [AppComponent]
})
export class AppModule { }
