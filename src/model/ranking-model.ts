export interface RankingModel {
    rank:number;
    prevRank:number;
    name:string;
    league:string;
    off:number;
    def:number;
    spi:number;


}
