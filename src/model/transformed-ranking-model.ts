export interface TransformedRankingModel {

    name:String;
    series : Array<SeriesData>;
}

export interface SeriesData{
    name : String;
    value : number;
}